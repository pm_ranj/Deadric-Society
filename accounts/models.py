from django.db import models

# Create your models here.
class post(models.Model):
    text = models.TextField(max_length=400, verbose_name='thought')
    date = models.DateTimeField(auto_now=True)
    people_tagged = models.ManyToManyField()